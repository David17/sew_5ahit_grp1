﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void sendRequestButton_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("127.0.0.1", 2055);
            try
            {
                Stream stream = client.GetStream();

                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);

                writer.AutoFlush = true;

                writer.WriteLine(requestTextBox.Text);
                TreeNode node = new TreeNode();
                XElement elem = XElement.Parse(reader.ReadToEnd());

                node = ToTreeNode(elem);
                fileTreeView.Nodes.Add(node);
            }
            catch (Exception)
            {
            }

        }

        public static TreeNode ToTreeNode(XElement elem)
        {
            return new TreeNode(elem.Name + ": " + elem.Attribute("Name").Value,
                                    elem.Elements().Select(e => ToTreeNode(e)).ToArray());
        }

        
        private void fileTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TcpClient client = new TcpClient("127.0.0.1", 2055);
            try
            {
                Stream stream = client.GetStream();

                StreamReader reader = new StreamReader(stream);
                StreamWriter writer = new StreamWriter(stream);

                writer.AutoFlush = true; 
                
                if(e.Node.Nodes.Count != 0)
                {
                    writer.WriteLine(e.Node.FullPath);
                    var dirs = new System.IO.DirectoryInfo(e.Node.FullPath);

                    foreach(var f in dirs.GetFiles("*.*"))
                    {
                        ListViewItem lv = this.listView1.Items.Add(f.Name);
                        lv = this.listView1.Items.Add(f.Name);
                        lv.SubItems.Add(f.CreationTime.ToString());
                    }

                }
    
            }
            catch (Exception)
            {
            }

           
        }
    }
}
