﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Net;

namespace Server
{
    class MainClass
    {
        static TcpListener listener;
        const int LIMIT = 5;

        public static void Main()
        {
            listener = new TcpListener(IPAddress.Any, 2055);
            listener.Start();
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamWriter sw = new StreamWriter(s);
                    StreamReader sr = new StreamReader(s);

                    sw.AutoFlush = true;
                    string request = sr.ReadLine();

                    try
                    {
                        Console.WriteLine(request);
                        DirectoryInfo dirInfo = new DirectoryInfo(request);
                        sw.Write(dirInfo.ToXML().ToString());
                        sw.Close();
                    }
                    catch (Exception e)
                    {
                        sw.Write("error: " + e.Message);
                    }

                    s.Close();
                }
                catch (Exception)
                {
                }
                soc.Close();
            }
        }
    }

    public static class Extenzione
    {
        public static XElement ToXML(this DirectoryInfo dir)
        {
            return new XElement("Directory",
                new XAttribute("Name", dir.Name),
                dir.GetDirectories().Select(d =>
                {
                    try { return ToXML(d); }
                    catch { return new XElement("Directory", 
                                new XAttribute("Name", d.Name),
                                new XAttribute("Error", "error reading directory")); 
                    }
                }),
                dir.GetFiles().Select(f =>
                {
                    try { return new XElement("File", 
                                new XAttribute("Name", f.Name),
                                new XAttribute("Length", f.Length)); }
                    catch { return null; }
                }));
        }

    }
}
