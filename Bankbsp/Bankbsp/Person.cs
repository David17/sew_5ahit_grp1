﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankbsp
{
    class Person:IComparable
    {
        public string Familienname { get; set; }
        public string Vorname { get; set; }

        public int CompareTo(object other)
        {
            Person anderer = (Person)other;

            return anderer.CompareTo(other);
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
