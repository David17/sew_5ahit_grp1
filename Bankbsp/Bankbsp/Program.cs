﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankbsp
{
    class Program
    {
        static void Main(string[] args)
        {
            Konto k1 = new Konto
            {
                Betrag = 100,
                Inhaber = new Person { Vorname = "Herwig", Familienname = "Macho" },
                Nr = "AT01 6546 1236 4477 3669"
                };
            Konto k2 = new Konto
            {
                Betrag = 1000,
                Inhaber = new Person { Vorname = "Susi", Familienname = "Macho" },
                Nr = "AT99 6546 4776 3321 1234"
            };
            Konto k3 = new Konto
            {
                Betrag = 500,
                Inhaber = new Person { Vorname = "Susi", Familienname = "Meier" },
                Nr = "AT45 3665 2334 1145 0002"
            };
            Geschäftskonto k4 = new Geschäftskonto
            {
                Betrag = 200,
                Inhaber = new Person { Vorname = "Max", Familienname = "Musterman" },
                Nr = "AT55 4665 7334 1145 0002",
                Ueberziehungsrahmen = 500
            };
            Sparkonto k5 = new Sparkonto
            {
                Betrag = 700,
                Inhaber = new Person { Vorname = "Fredl", Familienname = "Huber" },
                Nr = "AT75 3665 7334 1145 0002",
                Sparzinssatz = 0.03
            };
            Bank Raika = new Bank { Bezeichnung = "Raika Krems", Kunden = new List<Konto>() { k2, k1, k3, k4,k5 } };
            Console.WriteLine(Raika);
            Raika.Abschluss();
            Console.WriteLine(Raika);
            Raika.Sort(); // Sortieren mit IComparable von Konto
            Console.WriteLine(Raika);
            Raika.Kunden.Sort();
            Console.WriteLine(Raika);
            k4.Abhebung(1000);
            k4.Abhebung(200);
            Raika.Ueberweisung(30, k1, k2);
            Console.WriteLine(Raika);
            k5.BerechneBonusZinssatz();
            Raika.Ueberweisung(700, k4, k3);
            Console.WriteLine(Raika);
        }
    }
}
