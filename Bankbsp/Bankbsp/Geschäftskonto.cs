﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankbsp
{
    class Geschäftskonto:Konto
    {
        public double Ueberziehungsrahmen { get; set; }

        public override bool Abhebung(double betrag)
        {
            if((Betrag - betrag)> -Ueberziehungsrahmen)
            {
                base.Abhebung(betrag);
                return true;
            }
            else
            { 
                Console.WriteLine("Überziehungsrahmen überschritten!");
                return false;
            }
        }
    }
}
