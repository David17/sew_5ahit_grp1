﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankbsp
{
    class Konto:IComparable
    {
        public double Betrag { get; set; }
        public string Nr { get; set; }
        static internal double Zinssatz { get; set; }

        public Person Inhaber { get; set; }

        public virtual bool Abhebung(double betrag)
        {
            Betrag -= betrag;
            return true;
        }
        public int CompareTo(object other)
        {
            string compare = ((Konto)other).Nr;
            return Nr.CompareTo(compare);
        }
        public void Einzahlung(double betrag)
        {
            Betrag += betrag;
        }
        public Konto()
        {
            Zinssatz = 0.01;
        }

        public override string ToString()
        {
            return base.ToString();
        }
        public static void Ueberweisung(double betrag, Konto k1, Konto k2)
        {
            k1.Betrag -= betrag;
            k2.Betrag += betrag;
        }
        public static void Zinsen(double neu)
        {
            Zinssatz = neu;
        }
    }
}
