﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankbsp
{
    class Bank
    {
        public string Bezeichnung { get; set;}

        public List<Konto> Kunden = new List<Konto>();

        public Konto this[int i]
        {
            get { return Kunden[i]; }
            set { Kunden[i] = value; }
        }

        public void Abschluss()
        {
            for(int i=0; i<Kunden.Count; i++)
            {
                Kunden[i].Betrag = Kunden[i].Betrag +(Kunden[i].Betrag * Konto.Zinssatz);
            }
        }
        public void Add(Konto neu)
        {
            Kunden.Add(neu);
        }
        public void Add(List<Konto> neu)
        {
            neu = new List<Konto>();
        }
        public void Sort()
        {
            Kunden.Sort();
        }
        public void Ueberweisung(double betrag, Konto k1, Konto k2)
        {
           bool success = k1.Abhebung(betrag);
            if(success)
            k2.Einzahlung(betrag);
            else
                Console.WriteLine("Überweisung fehlgeschlagen");
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Kunden.Count; i++)
            {
                sb.Append(Kunden[i].Nr + "\n" + Kunden[i].Inhaber.Vorname + " " + Kunden[i].Inhaber.Familienname + "\n" + +Kunden[i].Betrag +"\n");
            }
            return sb.ToString();
        }
    }
}
