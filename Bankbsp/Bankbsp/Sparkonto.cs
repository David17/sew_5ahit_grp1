﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bankbsp
{
    class Sparkonto:Konto
    {
        public double Sparzinssatz { get; set; }
        
        public void BerechneBonusZinssatz()
        {
            Betrag *= (1 + Sparzinssatz);
        }
        public override bool Abhebung(double betrag)
        {
            if (Betrag - betrag > 0)
            {
                base.Abhebung(betrag);
                return true;
            }
            else
            {
                Console.WriteLine("Kann nicht kleiner 0 sein");
                return false;
            }
        }
    }
}
