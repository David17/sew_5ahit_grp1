﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raumschiff
{
    class Raumschiff
    {
        public byte Lebenspunkte { get; set; }
        public Ausstattung ausstattung { get; set; }

        public void Treffer(int stärke)
        {
        }
        public int CompareTo(object other)
        {
            Raumschiff anderes = (other as Raumschiff);
            Raumschiff zwei = (Raumschiff)other;
            //ned schön:
            if (Lebenspunkte > anderes.Lebenspunkte)
                return 1;
            if (Lebenspunkte < anderes.Lebenspunkte)
                return -1;
            return 0; //wenn gleich

            //schöner:
            //return Lebenspunkte.CompareTo(anderes.Lebenspunkte);
        }
    }
    class Ausstattung
    {
        public int Equipment{ get; set; }//des mit 0..50 programmieren
        public int Waffe { get; set; }
        public int Panzerung { get; set; }
    }
    class Versorgungsschiff : Raumschiff
    {
        public void repariere()
        {
        }
    }
    class Kampfschiff : Raumschiff
    {
    }
    class Flotte
    {
        List<Raumschiff> ListRaumschiff = new List<Raumschiff>();
    }
    class Starwar
    {
        Flotte f1;
        Flotte f2;

        public void fight_to_end()
        {
        }
    }
}
