﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_Nummernschild
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }
        public PictureBox p = new PictureBox();

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Image img = Image.FromFile("niederosterrich_wappen.jpg");
            Nummernschild n = new Nummernschild {Land="A", Bezirk = "ZT", Kennz = "AE23", Wappen = img };
            
          
            n.Draw(g, p);
            Controls.Add(p);
            //Rectangle schild = new Rectangle(80,80,260,50);
            //Rectangle viereck = new Rectangle(80,84,30,42);

            //Font f = new Font("myFont", 30, FontStyle.Regular);

            //Brush b = Brushes.White;
            //Brush b1 = Brushes.Red;
            //Brush b2 = Brushes.DarkBlue;
            //Brush b3 = Brushes.Black;
            //g.DrawRectangle(new Pen(b,1),schild);
            //g.FillRectangle(b, schild);
            //g.DrawLine(new Pen(b1, 1), new Point(80, 80), new Point(340, 80));
            //g.DrawLine(new Pen(b1, 1), new Point(80, 82), new Point(340, 82));
            //g.DrawLine(new Pen(b1, 1), new Point(80, 128), new Point(340, 128));
            //g.DrawLine(new Pen(b1, 1), new Point(80, 130), new Point(340, 130));
            //g.DrawRectangle(new Pen(b2, 1), viereck);
            //g.FillRectangle(b2, viereck);
            //g.DrawString("A", DefaultFont, b, 90, 110);
            //g.DrawString("KR", f, b3, 115, 82);
            //g.DrawString("123 A",f, b3, 225, 82);

            //Image img = Image.FromFile("niederosterrich_wappen.jpg");
            //PictureBox p = new PictureBox();
            //p.Image = img;
            //p.SizeMode = PictureBoxSizeMode.Zoom;
            //p.SetBounds(185, 88, 50, 35);
            //p.BackColor = Color.White;
            //Controls.Add(p);
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {

        }
    }
}
