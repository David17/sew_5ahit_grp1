﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_Nummernschild
{
    class Nummernschild
    {
        public string Land { get; set; }
        public string Bezirk { get; set; }
        public string Kennz { get; set; }
        public Image Wappen { get; set; }       

        public void Draw(Graphics g, PictureBox p)
        {
            Rectangle schild = new Rectangle(80, 80, 260, 50);
            Rectangle viereck = new Rectangle(80, 84, 30, 42);

            Font f = new Font("myFont", 30, FontStyle.Regular);

            Brush b = Brushes.White;
            Brush b1 = Brushes.Red;
            Brush b2 = Brushes.DarkBlue;
            Brush b3 = Brushes.Black;
            g.DrawRectangle(new Pen(b, 1), schild);
            g.FillRectangle(b, schild);
            g.DrawLine(new Pen(b1, 1), new Point(80, 80), new Point(340, 80));
            g.DrawLine(new Pen(b1, 1), new Point(80, 82), new Point(340, 82));
            g.DrawLine(new Pen(b1, 1), new Point(80, 128), new Point(340, 128));
            g.DrawLine(new Pen(b1, 1), new Point(80, 130), new Point(340, 130));
            g.DrawRectangle(new Pen(b2, 1), viereck);
            g.FillRectangle(b2, viereck);
            g.DrawString(Land, Form1.DefaultFont, b, 90, 110);
            g.DrawString(Bezirk, f, b3, 115, 82);
            g.DrawString(Kennz, f, b3, 225, 82);

            p.Image = Wappen;
            p.SizeMode = PictureBoxSizeMode.Zoom;
            p.SetBounds(175, 88, 50, 35);
            p.BackColor = Color.White;
            
            
        }
    }
}

