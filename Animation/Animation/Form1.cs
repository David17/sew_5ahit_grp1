﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Animation
{
    public partial class Form1 : Form
    {
        List<Panel> panels = new List<Panel>();
        TableLayoutPanel tp = new TableLayoutPanel();
        int row;
        int column;
        Color c;

        public Form1()
        {
            InitializeComponent();
            //createPanels();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        void createPanels()
        {
            tp.RowCount = row;
            tp.ColumnCount = column;
            tp.Width = 500;
            tp.Height = 500;
            tp.BackColor = Color.Gray;
            Controls.Add(tp);

            for (int i = 0; i < tp.RowCount * tp.ColumnCount; i++)
            {
                Panel p = new Panel();
                p.Click += panel_Click;
                p.Height = 70;
                p.Width = 70;
                p.BackColor = Color.LightGray;
                tp.Controls.Add(p);
            }
            this.Refresh();
        }

        void panel_Click(object sender, EventArgs e)
        {
            Timer t = new Timer();
            (sender as Panel).BackColor = c;
            if (panels.Count == 5)
            {
                panels.Remove(panels.First());
                panels.Add((sender as Panel));
                this.Refresh();
            }
            else
                panels.Add((sender as Panel));

            t.Interval = 5000; // Intervall festlegen
            t.Tick += new EventHandler(t_Tick); // Eventhandler ezeugen der beim Timerablauf aufgerufen wird
            t.Tag = sender;
            t.Start(); // Timer starten
        }
        void t_Tick(object sender, EventArgs e)
        {
            Timer t1 = (sender as Timer);
            Panel p = (t1.Tag as Panel);
            p.BackColor = Color.LightGray;
            
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options o = Options.getInstance;
            DialogResult r = o.ShowDialog();
            if(r == DialogResult.OK)
            {
                row = o.Rows;
                column = o.Colums;
                c = o.C;
                createPanels();
            }

        }
    }
}
