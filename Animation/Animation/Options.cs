﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Animation
{
    public partial class Options : Form
    {
        public int Rows { get; set; }
        public int Colums { get; set; }
        public Color C { get; set; }

        public Options()
        {
           InitializeComponent();

           try
           {
               using (StreamReader sr = new StreamReader("options.txt"))
               {
                   string data = sr.ReadToEnd();

                   string[] daten = data.Split(';');

                   Rows = Convert.ToInt32(daten[0]);
                   Colums = Convert.ToInt32(daten[1]);
                   try
                   {
                       C = Color.FromName(daten[2]);
                   }
                   catch
                   {
                       C = Color.Black;
                   }
               }
           }
           catch
           {
               Rows = 5;
               Colums = 5;
               C = Color.Black;
           }
        }
        private static Options o = null;

        public static Options getInstance
        {
            get
            {
                if(o == null)
                {
                    o = new Options();
                }
                return o;
            }
        }

        private void Options_Load(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            string rows = textBox1.Text;
            string columns = textBox2.Text;
            string color = textBox3.Text;
            

            using (StreamWriter sw = new StreamWriter("options.txt"))
            {
                sw.Write(rows + ";");
                sw.Write(columns + ";");
                sw.Write(color + ";");

            }



        }

    }
}
