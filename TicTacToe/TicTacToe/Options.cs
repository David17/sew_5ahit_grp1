﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }
        private void Options_Load(object sender, EventArgs e)
        {          
        }
        public string file1;
        public string file2;
        private void button1_Click(object sender, EventArgs e)
        {
            string path = "option.txt";
            StreamWriter sw = new StreamWriter(path);
            sw.AutoFlush= true;
            sw.Write(textBox1.Text+";");
            sw.Write(textBox2.Text+";");
            sw.Write(textBox3.Text+";");
            sw.Write(file1+";");
            sw.Write(file2);
            sw.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            file1 = openFileDialog1.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            file2 = openFileDialog2.FileName;
        }
    }
}
