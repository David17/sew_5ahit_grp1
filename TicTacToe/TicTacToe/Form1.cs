﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {           
        }
       TableLayoutPanel tlp = new TableLayoutPanel();
       Logic l;
       
        public void CreateFeld(int anz,int width, int heigth)
        {
            tlp.Controls.Clear();
            tlp.Width = 600;
            tlp.Height = 600;

            tlp.Location = new System.Drawing.Point(150, 150);

            tlp.ColumnCount = anz;
            tlp.RowCount = anz;

            this.Controls.Add(tlp);
            for (int i = 0; i < tlp.RowCount*tlp.ColumnCount; i++)
            {
                //Panel panel = new Panel();
                //panel.Click += panel_Click;
                //panel.BackColor = Color.Gray;
                //panel.Width = 95;
                //panel.Height = 95;
                PictureBox pb = new PictureBox();
                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                pb.Click += pb_Click;
                pb.Width = width;
                pb.Height = heigth;
                pb.BackColor = Color.Gray;

                tlp.Controls.Add(pb);   
            }
        }

        void pb_Click(object sender, EventArgs e)
        {
            var pp = tlp.GetPositionFromControl(sender as PictureBox);
            //MessageBox.Show("column " + pp.Column.ToString() + ", row " + pp.ToString());

            switch (l.Clicked(pp.Column, pp.Row))
            {
                case 0: MessageBox.Show("Bitte wählen sie ein leeres Feld aus");
                    break;
                case 1: (sender as PictureBox).Image = Image.FromFile(spieler1_img);
                    if (l.Win() == true)
                    {
                        MessageBox.Show("Spieler 1 hat gewonnen!");
                        tlp.Visible = false;
                    }
                    break;
                case 2: (sender as PictureBox).Image = Image.FromFile(spieler2_img);
                    if (l.Win())
                    {
                        MessageBox.Show("Spieler 2 hat gewonnen!");
                        tlp.Visible = false;
                    }
                    break;
            }            
        }
        //void panel_Click(object sender, EventArgs e)
        //{
        //        var pp = tlp.GetPositionFromControl(sender as Panel);
        //        //MessageBox.Show("column " + pp.Column.ToString() + ", row " + pp.ToString());

        //    switch(l.Clicked(pp.Column,pp.Row))
        //    {
        //        case 0: MessageBox.Show("Bitte wählen sie ein leeres Feld aus");
        //            break;
        //        case 1: (sender as Panel).BackColor = Color.Yellow;
        //            if (l.Win() == true)
        //            {
        //                MessageBox.Show("Spieler 1 hat gewonnen!");
        //                tlp.Visible = false;
        //            }
        //            break;
        //        case 2: (sender as Panel).BackColor = Color.Blue;
        //            if (l.Win())
        //            {
        //                MessageBox.Show("Spieler 2 hat gewonnen!");
        //                tlp.Visible = false;
        //            }
        //            break;
        //    }            
        //}
        public string spieler1_img;
        public string spieler2_img;
        private void button1_Click(object sender, EventArgs e)
        {
            string path = "option.txt";
            StreamReader sr = new StreamReader(path);

            string data = sr.ReadToEnd();

            sr.Close();           

            string[] splitdata = data.Split(';');
            int width = Int32.Parse(splitdata[0]);
            int heigth = Int32.Parse(splitdata[1]);
            int anz = Int32.Parse(splitdata[2]);
            spieler1_img = splitdata[3];
            spieler2_img = splitdata[4];
            
            CreateFeld(anz,width,heigth);
            l =new Logic(anz);
            tlp.Visible = true;
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options o = new Options();
            DialogResult r = o.ShowDialog();
            
          


        }    
    }
}
